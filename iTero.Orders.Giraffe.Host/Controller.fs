namespace iTero.Orders.Giraffe.Host.Controller

open Microsoft.AspNetCore.Http
open AutoMapper;
open iTero.Orders.Application.Entities.Commands;
open iTero.Orders.WebApi.Contract.Requests;

open System
open FSharp.Control.Tasks.V2.ContextInsensitive
open Giraffe
open iTero.Orders.Giraffe.Host.OrderRegistration.OrderRegistration

module Controller =
    let registerOrder: HttpHandler =
        fun (next: HttpFunc) (ctx: HttpContext) ->
            task {
                try
                    let! request = ctx.BindJsonAsync<OrderRegistrationRequest>()
                    let mapper = ctx.GetService<IMapper>()
                    let pipeline = ctx.GetService<OrderRegistrationPipeline>()
                    let cmd = mapper.Map<OrderRegistrationCommand>(request)
                    let! res = pipeline cmd

                    match res with
                    | Ok r -> return! Successful.OK r next ctx
                    | Error e -> return! RequestErrors.BAD_REQUEST e next ctx
                with
                | ex ->  return! RequestErrors.BAD_REQUEST ex next ctx
            }
