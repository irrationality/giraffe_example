namespace iTero.Orders.Giraffe.Host.OrderRegistration
open System
open iTero.Orders.Application.Abstractions;
open iTero.Orders.Application.Entities.Commands;
open System.Threading;
open iTero.Orders.Application.Entities;
open FSharp.Control.Tasks.V2
open iTero.Orders.Giraffe.Host.Pipelining.Pipelining
open AutoMapper
open iTero.Orders.Application.Entities.Queries
open iTero.Orders.Domain.Entities
open System.Threading.Tasks
open Microsoft.Extensions.DependencyInjection
open iTero.Orders.Giraffe.Host.MessagesPiping.MessagesPiping
open iTero.Orders.Giraffe.Host.CasePartnerLinks

module OrderRegistration =
    let validateData
        (matDbReadOnlyRepository: IMatDbReadOnlyRepository)
        (request: OrderRegistrationCommand): PipedTask<OrderRegistrationCommand, OrderRegistrationResult, string> = task {

        let! resourceId = matDbReadOnlyRepository.CheckIfCompanyContactAndResourceAreLinkedAsync
                              (request.Sender, request.PartnerId, request.ContactId, CancellationToken.None)
        if not resourceId.HasValue then
            return Handled(Error "invalid data")
        else
        request.ResourceId <- resourceId.Value;
        return Proceed request
    }

    let handle
        (ordersRepository: IOrdersRepository)
        (mapper: IMapper)
        (eventLogRepository: IEventLogRepository)
        (getCasePartnerLinks: GetCasePartnerLinks)
        (cmd: OrderRegistrationCommand) = task {

        let newOrder = mapper.Map<NewOrder>(cmd)
        let! casePartnerLinks = getCasePartnerLinks (GetCasePartnerLinksQuery (Order = newOrder, CurrentWorkType = WorkType.Order_Registration))

        let! eventId = task {
            if cmd.UncommitedEvent = null then
                let id = Guid.NewGuid()
                do! eventLogRepository.PrepareEventAsync(cmd.Id, id);
                return id
            else
                return Guid.Parse(cmd.UncommitedEvent.Id) }
        let! savedOrder = ordersRepository.RegisterOrderAsync(newOrder, eventId, casePartnerLinks |> Seq.toArray)
        let orderRegistrationResult =
            mapper.Map<OrderRegistrationResult>
                (savedOrder, fun opts -> opts.Items.Add("Detail", Seq.head savedOrder.CaseOrderDetails))

        orderRegistrationResult.Sender <- cmd.Sender
        orderRegistrationResult.ResourceId <- cmd.ResourceId
        do! eventLogRepository.CommitEventAsync(orderRegistrationResult, eventId)
        return Ok orderRegistrationResult
    }

    let handleConsistencyFailover
        (ordersRepository: IOrdersRepository)
        (mapper: IMapper)
        (eventLogRepository: IEventLogRepository)
        (cmd: OrderRegistrationCommand): PipedTask<OrderRegistrationCommand, OrderRegistrationResult, string> = task {

        if cmd.UncommitedEvent = null then
            return Proceed cmd
        else
        let! order = ordersRepository.GetOrderByEventIdAsync(cmd.UncommitedEvent.Id, EventType.OrderRegistered)

        if not (order :? RegisteredOrder) then
            return Proceed cmd
        else
        let orderRegistrationResult = mapper.Map<OrderRegistrationResult>
                                          (order, fun opts -> opts.Items.Add("Detail", Seq.head order.CaseOrderDetails))

        orderRegistrationResult.Sender <- cmd.Sender
        orderRegistrationResult.ResourceId <- cmd.ResourceId
        do! eventLogRepository.CommitEventAsync(orderRegistrationResult, Guid.Parse(cmd.UncommitedEvent.Id))
        return Handled(Ok orderRegistrationResult)
    }

    let bootstrapOrderRegistrationPipeline (provider: IServiceProvider) =
        let matDbReadOnlyRepository = provider.GetService<IMatDbReadOnlyRepository>()
        let eventLogRepository = provider.GetService<IEventLogRepository>()
        let ordersRepository = provider.GetService<IOrdersRepository>()
        let mapper = provider.GetService<IMapper>()
        let getCasePartnerLinks = provider.GetService<GetCasePartnerLinks>()
        let messagePipeline = bootstrapMessagePiping provider
        let validateData = validateData matDbReadOnlyRepository
        let handle = handle ordersRepository mapper eventLogRepository getCasePartnerLinks
        let handleConsistencyFailover = handleConsistencyFailover ordersRepository mapper eventLogRepository

        let pipe = fun cmd -> cmd |> (messagePipeline >=> validateData >=> handleConsistencyFailover) |> map handle
        pipe

    type OrderRegistrationPipeline = OrderRegistrationCommand -> Task<Result<OrderRegistrationResult, string>>





