﻿open System
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Hosting
open Microsoft.Extensions.DependencyInjection
open Giraffe
open iTero.Extensions.DependencyInjection
open iTero.Orders.Application;
open iTero.Orders.Application.Mapping
open iTero.Orders.DynamoDbRepository
open iTero.Orders.DynamoDbRepository.Mapping
open iTero.Orders.RxServiceClient
open iTero.Orders.WebApi.Utils
open AutoMapper
open System.Reflection
open iTero.Orders.WebApi.Mapping
open iTero.Orders.EntityFrameworkRepository.Mapping
open MediatR
open Microsoft.Extensions.Configuration;
open iTero.Orders.Application.Configuration
open iTero.Orders.DynamoDbRepository.Configuration
open iTero.Orders.EntityFrameworkRepository.Configuration
open iTero.Orders.ReadOnlyPostgresRepository.Configuration
open iTero.Application.Abstractions
open iTero.Orders.WebApi.Contract
open iTero.Hosting
open iTero.Orders.Giraffe.Host
open iTero.Orders.Giraffe.Host.OrderRegistration.OrderRegistration
open iTero.Orders.Giraffe.Host.Controller
open iTero.Orders.Giraffe.Host.CasePartnerLinks


let webApp =
    choose [
        route "/ping" >=> text "pong"
        route "/" >=> htmlFile "/pages/index.html"
        POST >=> choose [
            route "/api/events/OrderRegistration" >=> Controller.registerOrder
        ]

    ]

let configureAppConfiguration (context: WebHostBuilderContext) (config: IConfigurationBuilder) =
    config
        .AddJsonFile("appsettings.json", false, true)
        .AddJsonFile(sprintf "appsettings.%s.json" context.HostingEnvironment.EnvironmentName, true)
        .AddEnvironmentVariables() |> ignore

let configureApp (app: IApplicationBuilder) =
    app.UseGiraffe webApp


let configureServices (services: IServiceCollection) =
    let builder = new ConfigurationBuilder()
    let config = builder
                    .AddJsonFile("appsettings.json", false, true)
                    .AddEnvironmentVariables()
                    .Build()
    services
        .Configure<ApplicationConfig>(config.GetSection("ApplicationConfig"))
        .Configure<DynamoDbRepositoryConfig>(config.GetSection("DynamoDbRepositoryConfig"))
        .Configure<OrdersDatabaseContextConfig>(config.GetSection("OrdersDatabaseContextConfig"))
        .Configure<ReadOnlyDatabaseContextConfig>(config.GetSection("ReadOnlyDatabaseContextConfig"))
        .Configure<RxServiceClientConfig>(config.GetSection("RxServiceClientConfig"))
        .AddOrdersRepository()
        .AddEventRegistrationService()
        .AddEventsRepository()
        .AddReadOnlyDbContext()
        .AddRxServiceClientContext()
        .AddScoped<ExceptionHandlerFilterAttribute>()
        .AddScoped<IPropertyScopeManager<FlowPhaseDescriptor>, StackPropertyScopeManager<FlowPhaseDescriptor>>()
        .AddAutoMapper(
            Assembly.GetExecutingAssembly(),
            typeof<EventsMappingProfile>.Assembly,
            typeof<ApplicationMappingProfile>.Assembly,
            typeof<DynamoMappingProfile>.Assembly,
            typeof<DataAccessMappingProfile>.Assembly)
        .AddGiraffe()
        .AddMediatR(Assembly.GetExecutingAssembly(), typeof<ApplicationMappingProfile>.Assembly)
        .AddTransient<OrderRegistrationPipeline>(System.Func<_, _>(bootstrapOrderRegistrationPipeline))
        .AddTransient<GetCasePartnerLinks>(System.Func<_, _>(CasePartnerLinks.bootstrap))
    |> ignore


[<EntryPoint>]
let main _ =
    WebHostBuilder()
        .UseKestrel()
        .Configure(Action<IApplicationBuilder> configureApp)
        .ConfigureServices(configureServices)
        .Build()
        .Run()
    0
