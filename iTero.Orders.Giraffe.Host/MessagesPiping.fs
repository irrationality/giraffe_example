namespace iTero.Orders.Giraffe.Host.MessagesPiping
open iTero.Orders.Giraffe.Host.Pipelining
open System;
open System.Threading.Tasks;
open AutoMapper;
open iTero.Orders.Application.Abstractions;
open iTero.Orders.Application.Entities;
open iTero.Orders.Application.Entities.Commands;
open iTero.Orders.Application.Entities.Messages;
open Newtonsoft.Json;
open FSharp.Control.Tasks.V2
open Microsoft.Extensions.DependencyInjection

module MessagesPiping =
    open Pipelining

    let MessageTimeoutDays = 30
    let validation
        (messageLogRepository: IMessageLogRepository)
        (eventLogRepository: IEventLogRepository)
        (mapper: IMapper)
        (request: 'TCommand :> BaseCommand): PipedTask<'TCommand, 'TResponse, string> = task {
            let message = mapper.Map<Message>(request)
            if DateTime.UtcNow.Subtract(message.CreationDate).Days >= MessageTimeoutDays then
                return Handled(Error(sprintf "timeout exceed %d days" MessageTimeoutDays))
            else

            let! loggedMessage = messageLogRepository.GetByIdAsync(message.Id)
            if loggedMessage = null then
                return Proceed request
            else
            let baseCmd = (request :> BaseCommand)
            baseCmd.AlreadyLogged <- true

            if message.HashCode <> loggedMessage.HashCode then
                return Handled(Error "message hash collision")
            else
            let eventT = eventLogRepository.GetEventByMessageIdAsync(message.Id)
            do! Task.WhenAll(eventT, messageLogRepository.IncrementRetriesCount(message.Id))
            let! event = eventT

            if event<>null && event.State = EventState.Commited then
                return Handled(Ok(JsonConvert.DeserializeObject<'Tresponse>(event.PayloadJson)))
            else
            baseCmd.UncommitedEvent <- event
            return Proceed request
        }



    let logging
        (messageLogRepository: IMessageLogRepository)
        (mapper: IMapper)
        (request: 'TCommand :> BaseCommand): PipedTask<'TCommand, 'TResponse, string> = task {

            let baseCmd = (request :> BaseCommand)
            if baseCmd.AlreadyLogged then
                return Proceed request
            else
            let newMessage = mapper.Map<Message>(request)
            do! messageLogRepository.SaveAsync(newMessage)
            return Proceed request
        }

    type MessagePipeChunk<'TCommand, 'TResponse> = 'TCommand -> PipedTask<'TCommand, 'TResponse, string>

    let bootstrapMessagePiping
        (provider:IServiceProvider)
        : MessagePipeChunk<'TCommand, 'TResponse> =
            let messageLogRepository = provider.GetService<IMessageLogRepository>()
            let eventLogRepository =  provider.GetService<IEventLogRepository>()
            let mapper =  provider.GetService<IMapper>()
            let validation = validation messageLogRepository  eventLogRepository mapper
            let logging = logging messageLogRepository   mapper
            let pipe  = Pipelining.combine validation logging
            pipe


    let bootstrapMessagePipingObj =  fun (provider:IServiceProvider) -> (bootstrapMessagePiping provider):> obj














