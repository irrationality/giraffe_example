namespace iTero.Orders.Giraffe.Host
open System
open System.Threading.Tasks;
open iTero.Orders.Application.Abstractions;
open iTero.Orders.Application.Entities.Queries;
open iTero.Orders.Domain.Entities;
open iTero.Orders.Domain.Entities.Enum;
open FSharp.Control.Tasks.V2
open Microsoft.Extensions.DependencyInjection

module rec CasePartnerLinks =
    type GetCasePartnerLinks = GetCasePartnerLinksQuery ->  Task<CasePartnerLink list>

    let bootstrap (provider: IServiceProvider) =
        let casePartnerLinkReadOnlyRepository = provider.GetService<ICasePartnerLinkReadOnlyRepository>()
        getCasePartnerLinks casePartnerLinkReadOnlyRepository


    let getCasePartnerLinks
        (casePartnerLinkReadOnlyRepository: ICasePartnerLinkReadOnlyRepository)
        (request: GetCasePartnerLinksQuery): Task<CasePartnerLink list> = task {

        if request.CurrentWorkType = WorkType.Scanning then
            return! (getForScanning casePartnerLinkReadOnlyRepository request)
        else
            return! (getForRegistration casePartnerLinkReadOnlyRepository request)
    }

    let private getForRegistration
        (casePartnerLinkReadOnlyRepository: ICasePartnerLinkReadOnlyRepository)
        (request: GetCasePartnerLinksQuery): Task<CasePartnerLink list> = task {
        let partnerId = request.Order.Partner.Id

        let initiatorPartnerTask = casePartnerLinkReadOnlyRepository.GetInitiatorPartnerAsync(partnerId)
        let distributorPartnerTask = casePartnerLinkReadOnlyRepository.GetDistributorPartnerAsync(partnerId)
        let! partners = Task.WhenAll(initiatorPartnerTask, distributorPartnerTask)

        let result = partners
                     |> Seq.filter (fun p -> p <> null)
                     |> Seq.map (fun p -> CasePartnerLink(0L, request.Order, p))
                     |> Seq.toList

        if (Seq.head request.Order.CaseOrderDetails).IsOrthoCase() then
            return result
        else
        let! orthoModellingPartner =
                casePartnerLinkReadOnlyRepository.GetOrthoModellingPartnerAsync(partnerId)
        return CasePartnerLink(0L, request.Order, orthoModellingPartner) :: result
    }

    let private getForScanning
        (casePartnerLinkReadOnlyRepository: ICasePartnerLinkReadOnlyRepository)
        (request: GetCasePartnerLinksQuery): Task<CasePartnerLink list> = task {
        let result: CasePartnerLink list = []
        let shipToPartner =
            request.Order.DigitalProductOrderDetails
            |> Seq.tryHead
            |> function
                | Some d -> d.ShipToPartner |> function
                                                | null -> new Partner(Partner.DEFAULT_PARTNER_ID, PartnerType.Lab)
                                                | x -> x
                | None -> new Partner(Partner.DEFAULT_PARTNER_ID, PartnerType.Lab)

        let result = new CasePartnerLink(0L, request.Order, shipToPartner) :: result

        let! interpretationSite = casePartnerLinkReadOnlyRepository.GetInterpretationPartnerAsync(shipToPartner.Id)
        let result = new CasePartnerLink(0L, request.Order, interpretationSite) :: result

        if (Seq.head request.Order.CaseOrderDetails).IsOrthoCase() then
            return result
        else
        let! millingPartner = getMillingPartnersAsync casePartnerLinkReadOnlyRepository shipToPartner.Id
        let result = new CasePartnerLink(0L, request.Order, millingPartner) :: result

        let! gCodePartner = casePartnerLinkReadOnlyRepository.GetGCodePartnerAsync(millingPartner.Id)
        let result = new CasePartnerLink(0L, request.Order, gCodePartner) :: result

        return result
    }

    let private getMillingPartnersAsync
        (casePartnerLinkReadOnlyRepository: ICasePartnerLinkReadOnlyRepository)
        (partnerId: int): Task<Partner> =
        task {
        let! relatedMillingPartner =
            casePartnerLinkReadOnlyRepository
                .GetRelatedPartnerAsync (partnerId, PartnerRelationsType.MillingSiteInternalLab_MillingSite, PartnerType.MillingSite)

        if relatedMillingPartner <> null then
             return relatedMillingPartner
        else
        let defaultMillingPartnerTask = casePartnerLinkReadOnlyRepository
                                                .GetDefaultPartnerAsync (PartnerType.MillingSite, GeneralSetting.DefaultMillingSitePartnerID);
        let! millingPartners = casePartnerLinkReadOnlyRepository.GetMillingPartnersAsync()
        if millingPartners |> Seq.sumBy (fun (struct (_, percentage)) -> percentage) > 100 then
           return raise (System.InvalidOperationException("Sum of percents more than 100"))
        else
        let randomValue = Random().Next(0, 100)
        let (_, chosenPartner) =
                  millingPartners |>
                  Seq.fold
                      (fun (threshold: int, chosenPartner: Partner) (struct (partner, percentage)) ->
                             if chosenPartner <> null then
                                 (threshold, chosenPartner)
                             else
                             let currentThreshold = threshold + percentage
                             if randomValue > currentThreshold then
                                (currentThreshold, null)
                             else
                                 (currentThreshold, partner))
                      (0, null)
        return match chosenPartner with
                | null -> defaultMillingPartnerTask.Result
                | _ -> chosenPartner

    }



