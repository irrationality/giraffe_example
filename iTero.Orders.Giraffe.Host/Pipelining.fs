namespace iTero.Orders.Giraffe.Host.Pipelining
open System.Threading.Tasks
open FSharp.Control.Tasks.V2

module Pipelining =
    type Piped<'TCommand, 'TResult, 'TError> =
        | Proceed of 'TCommand
        | Handled of Result<'TResult, 'TError>

    type PipedTask<'TCommand, 'TResult, 'TError> = Task<Piped<'TCommand, 'TResult, 'TError>>


    let  bind
        (f: 'TCommand -> PipedTask<'TCommand, 'TResult, 'TError>)
        (m: PipedTask<'TCommand, 'TResult, 'TError>):PipedTask<'TCommand, 'TResult, 'TError>  =
        task {
            let! piped = m
            let result =
                match piped with
                | Proceed cmd -> f cmd
                | _ -> m
            return! result
        }

    let combine (f1: 'TCommand -> PipedTask<'TCommand, 'TResult, 'TError>) (f2: 'TCommand -> PipedTask<'TCommand, 'TResult, 'TError>) =
        fun m -> bind f2 (f1 m)

    let (>=>) (f1: 'TCommand -> PipedTask<'TCommand, 'TResult, 'TError>) (f2: 'TCommand -> PipedTask<'TCommand, 'TResult, 'TError>) =
        fun m -> bind f2 (f1 m)

    let map  (f: 'TCommand -> Task<Result<'TResult, 'TError>>) (m: PipedTask<'TCommand, 'TResult, 'TError>)  =
        task {
             let! piped = m
             let! result =
                match piped with
                | Proceed cmd -> f cmd
                | Handled res -> res |> Task.FromResult
            return result
        }
